package com.example.al.supertroper;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.logging.Handler;
import java.util.logging.LogRecord;


/**
 * Created by al on 11/8/15.
 */
class MyViewPagerAdapter extends PagerAdapter {

    private boolean debug = true;
    private String STORY_OBJECT_KEY = "story_object_key";
    private String TAG = "MyViewPagerAdpater";
    public int lastIntegeratedBatch = 0;

    ListActivity activity;
    ArrayList<StoryObject> storyObjects;

    MyViewPagerAdapter(ListActivity activity) {
        this.activity = activity;
        initilizeStoryObjects();

    }

    public void addStoryObjectFromBatch(int number) {
        Utilities.getBatch(number, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (debug)
                    Log.d(TAG, response.toString());
                String[] iterator = {"two", "three", "four", "five"};
                try {
                    JSONObject jsonObject = response.getJSONObject(0);
                    StoryObject storyObject = new StoryObject(jsonObject, jsonObject.getString("blurredUrl"));
                    storyObjects.add(storyObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {

                    for (int i = 1; i < response.length(); i++) {

                        storyObjects.add(new StoryObject(response.getJSONObject(i)));

                    }
                    lastIntegeratedBatch = Utilities.getLastBatchNumber();
                }
                catch (JSONException e){
                    e.printStackTrace();
                }

            }
        });
    }

    public void initilizeStoryObjects() {
        storyObjects = new ArrayList<StoryObject>();
        if (debug)
            Log.d(TAG, "initilize story object");
        addStoryObjectFromBatch(Utilities.getLastBatchNumber());
    }


    @Override
    public int getCount() {
        return storyObjects.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public float getPageHeight(int position) {
        return 1.f;
    }


    @Override
    public int getItemPosition(Object object) {
        Integer index = (Integer) ((View) object).getTag();
        if (debug) Log.d(TAG, "index in get item position is " + index);
        if (index == null) {
            return POSITION_UNCHANGED;
        }
        return index;

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        if (activity.shallNotPass)
            return null;
        View layout = getViewItemAt(position, container.getContext());
        container.addView(layout);

        if (debug)
            Log.d(TAG, "get position " + position);

        layout.setTag(position);


        return layout;


    }

    boolean isLoaded = false;

    public View getViewItemAt(final int position, Context context) {

        final StoryObject storyObject = storyObjects.get(position);
        if (debug)
            Log.d(TAG, "story object is " + storyObject.toString());


        final ImageView img = new ImageView(context);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        img.setScaleType(ImageView.ScaleType.CENTER_CROP);
        params.gravity = Gravity.CENTER;
        img.setLayoutParams(params);
        Log.d(TAG, storyObject.url);
        Glide.with(context).load(storyObject.url).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                e.printStackTrace();
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                isLoaded = true;
                Log.d(TAG, "loaded");
                return false;
            }
        }).crossFade().diskCacheStrategy(DiskCacheStrategy.RESULT).into(img);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (debug)
                    Log.d(TAG, "Clicked at " + position);
                if (activity.shallNotPass)
                    return;

                Intent intent = new Intent(activity, DetailActivity.class);
                intent.putExtra(STORY_OBJECT_KEY, storyObject.toString());

                activity.startActivity(intent);
            }
        });

        final FrameLayout layout = new FrameLayout(context);
//        TODO uncomment after php api
//        if (!isLoaded || position != 0)
//            layout.setBackgroundColor(Color.parseColor(storyObject.backgroundLoadingColor));
        layout.addView(img);

        final TextView mainText = new TextView(context);

        mainText.setTypeface(Utilities.getFontBold());
        mainText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 26);
        mainText.setText(storyObject.title);
        mainText.setTextColor(Color.WHITE);
        mainText.setBackgroundResource(R.drawable.text_border_style);
        FrameLayout.LayoutParams textParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        textParams.gravity = Gravity.CENTER_HORIZONTAL;
        textParams.topMargin = (int) (DeviceDimention.getScreenHeight() * (0.3));

        layout.addView(mainText, textParams);


        final TextView detailText = new TextView(context);

        detailText.setTypeface(Utilities.getFontLight());
        detailText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
        detailText.setText((storyObject.smallDetail));
        detailText.setTextColor(Color.WHITE);
        detailText.setGravity(Gravity.CENTER);
        FrameLayout.LayoutParams detailTextParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        detailTextParams.gravity = Gravity.CENTER_HORIZONTAL;
        detailTextParams.topMargin = (int) (DeviceDimention.getScreenHeight() * (0.5));

        layout.addView(detailText, detailTextParams);


        final TextView timeTextView = new TextView(context);

        timeTextView.setTypeface(Utilities.getFontLight());
        timeTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
        timeTextView.setText((storyObject.time));
        timeTextView.setTextColor(Color.WHITE);
        FrameLayout.LayoutParams timeTextViewParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        timeTextViewParams.gravity = Gravity.CENTER_HORIZONTAL;
        timeTextViewParams.topMargin = (int) (DeviceDimention.getScreenHeight() * (0.75));

        layout.addView(timeTextView, timeTextViewParams);


        if (position % 5 != 0) {
            return layout;
        }

        img.setVisibility(View.GONE);
        mainText.setVisibility(View.GONE);
        timeTextView.setVisibility(View.GONE);
        detailText.setVisibility(View.GONE);

        String bluredUrl = storyObject.blurredUrl;

        final ImageView blurView = new ImageView(context);

        blurView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        blurView.setLayoutParams(params);
        blurView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, DetailActivity.class);
                if (debug)
                    Log.d(TAG, "Clicked at " + position);
                if (activity.shallNotPass)
                    return;
                intent.putExtra(STORY_OBJECT_KEY, storyObject.toString());

                activity.startActivity(intent);
            }
        });


        Glide.with(context).load(bluredUrl).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                android.os.Handler handler = new android.os.Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        img.setVisibility(View.VISIBLE);
                        mainText.setVisibility(View.VISIBLE);
                        timeTextView.setVisibility(View.VISIBLE);
                        detailText.setVisibility(View.VISIBLE);
                    }
                }, 400);


                return false;
            }
        }).crossFade().diskCacheStrategy(DiskCacheStrategy.RESULT).into(blurView);

        layout.addView(blurView);

//        DragableLayout dragableLayout = new DragableLayout(context);
//        dragableLayout.onLayoutMovedListener = new DragableLayout.OnLayoutMovedListener() {
//            @Override
//            public void onChanged(float value) {
//                    blurView.setAlpha(value);
//
//            }
//        };
//        layout.addView(dragableLayout);
        activity.dragableLayout.onLayoutMovedListener = new DragableLayout.OnLayoutMovedListener() {
            @Override
            public void onChanged(float value) {
                if (activity.shallNotPass)
                    return;
                if (Build.VERSION.SDK_INT < 11) {
                    AlphaAnimation animation = new AlphaAnimation(value, value);
                    animation.setDuration(1);
                    animation.setFillAfter(true);
                    blurView.startAnimation(animation);
                } else
                    blurView.setAlpha(value);

            }
        };

        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((VerticalViewPager) container).removeView((View) object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return position + "";
    }
}
