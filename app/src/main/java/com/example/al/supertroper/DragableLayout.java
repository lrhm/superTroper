package com.example.al.supertroper;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by al on 11/8/15.
 */

public class DragableLayout extends RelativeLayout {

    private String TAG = "DragableLayout";
    private boolean debug = true;
    public int lastIntegeratedBatch = 0;

    public interface OnLayoutMovedListener {
        public void onChanged(float value);
    }

    OnLayoutMovedListener onLayoutMovedListener;
    // Multi line textview's Text must be equivalent in len(char) , use the space luck !

    TextView mainText, mainSubDetail, detailText;
    String main = "", subMain = "", detail = "";

    public void setTexts(String main, String subMain, String detail) {

        if (mainText != null) mainText.setText(main);
        if (mainSubDetail != null) mainSubDetail.setText(subMain);
        if (detailText != null) detailText.setText(detail.replace("endline" , "\n"));
        this.main = main;
        this.subMain = subMain;
        this.detail = detail.replace("endline" , "\n");


    }
    public void doThingsToJson(JSONObject jsonObject) {

        try {
            String titleOfDay = jsonObject.getString("titleOfToday");
            String subTitleOfToday = jsonObject.getString("subTitleOfToday");
            String detailOfToday = jsonObject.getString("detailOfToday");
            setTexts(titleOfDay , subTitleOfToday , detailOfToday);
            lastIntegeratedBatch = Utilities.getLastBatchNumber();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
    DragableLayout(Context context) {
        super(context);

//        Utilities.getLastBatch(new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//            doThingsToJson(response);
//            }
//        });

        RelativeLayout relativeLayout = new RelativeLayout(context);


        ImageView imgView = new ImageView(context);
        imgView.setScaleType(ImageView.ScaleType.FIT_START);
        imgView.setImageResource(getDrawableWithLessRatio());

        float alpha = 0.85f;

        if (Build.VERSION.SDK_INT < 11) {
            AlphaAnimation animation = new AlphaAnimation(alpha, alpha);
            animation.setDuration(1);
            animation.setFillAfter(true);
            imgView.startAnimation(animation);
        } else
            imgView.setAlpha(alpha);
        addView(imgView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));


        mainText = new TextView(context);

        mainText.setTypeface(Utilities.getFontBold());
        mainText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 26);
        mainText.setText(main);
        mainText.setGravity(Gravity.CENTER);
        mainText.setTextColor(Color.WHITE);
        mainText.setId(593);
        RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        textParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        textParams.topMargin = (int) (DeviceDimention.getScreenHeight() * (0.3));

        relativeLayout.addView(mainText, textParams);


        mainSubDetail = new TextView(context);
        mainSubDetail.setTypeface(Utilities.getFontLight());
        mainSubDetail.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
        mainSubDetail.setText(subMain);
        mainSubDetail.setTextColor(Color.parseColor("#f29076"));

        RelativeLayout.LayoutParams mainSubDetailParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mainSubDetailParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        mainSubDetailParams.addRule(RelativeLayout.BELOW, mainText.getId());

        relativeLayout.addView(mainSubDetail, mainSubDetailParams);


        addView(relativeLayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));


        detailText = new TextView(context);

        detailText.setTypeface(Utilities.getFontLight());
        detailText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
        detailText.setText((detail));
        detailText.setTextColor(Color.WHITE);
        detailText.setGravity(Gravity.CENTER);
        RelativeLayout.LayoutParams detailTextParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        detailTextParams.topMargin = (int) (DeviceDimention.getScreenHeight() * (0.58));
        detailTextParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        addView(detailText, detailTextParams);


    }

    public int getDrawableWithLessRatio(){

        float []ratios = {1920.f/1080 , 1024.f/768 , 960.f/640 , 800.f/400 };
        int []drawables ={R.drawable.news_cover_fhd , R.drawable.news_cover_c , R.drawable.news_cover_b , R.drawable.news_cover_a};

        float deviceRatio = DeviceDimention.getScreenHeight() / (float)DeviceDimention.getScreenWidth();

        float minRatio = Math.abs(ratios[0] - deviceRatio);
        int resDrawable = drawables[0];

        for (int i = 1 ; i < ratios.length ; i++){
            float temp = Math.abs(ratios[i] - deviceRatio);
            if (temp < minRatio){
                minRatio = temp;
                resDrawable = drawables[i];
            }
        }
        if(debug)
            Log.d(TAG, "device ratio is " + deviceRatio + " min ratio is " + minRatio);

        return resDrawable;


    }


    float prevY;
    float prevY2;


    @Override
    public void setLayoutParams(ViewGroup.LayoutParams params) {
        super.setLayoutParams(params);
        if (onLayoutMovedListener != null)
            onLayoutMovedListener.onChanged((((RelativeLayout.LayoutParams) params).topMargin + params.height) / (float) params.height);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        final RelativeLayout.LayoutParams dragParam = (RelativeLayout.LayoutParams) getLayoutParams();

//        Log.d("TAG" , event.getAction() + " is action");
        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE: {
                int dif = (int) (event.getRawY() - prevY);

                int temp = dragParam.topMargin + dif;
//                dragParam.topMargin += dif ;
//                Log.d("TAG", "ACTION MOVE");
                if (dif < 0) {
                    if (temp <= 0) {
                        dragParam.topMargin += dif;
                        setLayoutParams(dragParam);
                    }
                } else if (temp <= 0) {
                    dragParam.topMargin += dif;

                    setLayoutParams(dragParam);
                }

                prevY2 = prevY;
                prevY = event.getRawY();

                break;
            }

            case MotionEvent.ACTION_CANCEL: {

                float dif = (event.getRawY() - prevY2);
//                Log.d("TAG" , "Action Cancel + dif = "+ dif);
                dragParam.height = getHeight();

                if (dif > 0) {
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) DragableLayout.this.getLayoutParams();
                    final int firstTopMargin = params.topMargin;
                    if (firstTopMargin >= -5)
                        break;
                    Animation a = new Animation() {
                        @Override
                        protected void applyTransformation(float interpolatedTime, Transformation t) {
//                            Log.d("TAG", "first tp = " + firstTopMargin + " interpolatedTime " + interpolatedTime);
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) DragableLayout.this.getLayoutParams();
                            params.topMargin = (int) (-firstTopMargin * interpolatedTime + firstTopMargin);
                            DragableLayout.this.setLayoutParams(params);


                        }
                    };
                    a.setDuration(500);
                    startAnimation(a);
                } else if (dif < 0) {

                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) DragableLayout.this.getLayoutParams();
                    final int firstTopMargin = params.topMargin;
                    Animation a = new Animation() {
                        @Override
                        protected void applyTransformation(float interpolatedTime, Transformation t) {
//                            Log.d("TAG", "first tp = " + firstTopMargin + " interpolatedTime " + interpolatedTime);
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) DragableLayout.this.getLayoutParams();
                            params.topMargin = (int) ((-params.height - firstTopMargin + DeviceDimention.getScreenHeight() * 0.05) * interpolatedTime + firstTopMargin);
                            DragableLayout.this.setLayoutParams(params);


                        }
                    };
                    a.setDuration(500);
                    startAnimation(a);


                }
                prevY = event.getRawY();
                return false;

            }


            case MotionEvent.ACTION_UP: {
                float dif = (event.getRawY() - prevY2);
//                Log.d("TAG" , "Action UP + dif = "+ dif);
                dragParam.height = getHeight();


                if (dif > 0) {
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) DragableLayout.this.getLayoutParams();
                    final int firstTopMargin = params.topMargin;
                    if (firstTopMargin >= -5)
                        break;
                    Animation a = new Animation() {
                        @Override
                        protected void applyTransformation(float interpolatedTime, Transformation t) {
//                            Log.d("TAG", "first tp = " + firstTopMargin + " interpolatedTime " + interpolatedTime);
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) DragableLayout.this.getLayoutParams();
                            params.topMargin = (int) (-firstTopMargin * interpolatedTime + firstTopMargin);
                            DragableLayout.this.setLayoutParams(params);


                        }
                    };
                    a.setDuration(500);
                    startAnimation(a);
                } else if (dif < 0) {

                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) DragableLayout.this.getLayoutParams();
                    final int firstTopMargin = params.topMargin;
                    Animation a = new Animation() {
                        @Override
                        protected void applyTransformation(float interpolatedTime, Transformation t) {
//                            Log.d("TAG", "first tp = " + firstTopMargin + " interpolatedTime " + interpolatedTime);
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) DragableLayout.this.getLayoutParams();
                            params.topMargin = (int) ((-params.height - firstTopMargin + DeviceDimention.getScreenHeight() * 0.05) * interpolatedTime + firstTopMargin);
                            DragableLayout.this.setLayoutParams(params);


                        }
                    };
                    a.setDuration(500);
                    startAnimation(a);


                }
                prevY = event.getRawY();
                return false;


            }
            case MotionEvent.ACTION_DOWN: {

                prevY = event.getRawY();

                break;
            }
        }
        return true;
    }


}
